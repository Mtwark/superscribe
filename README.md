# Superscribe

Superscribe is an Android app designed to make tracking and managing subscription services simple. Subscription costs and benefits are shown at-a-glance within a clean and minimalistic UI, and future versions will include the ability to cancel, upgrade, and manage all of your subscriptions in one place.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

A step by step series of examples that tell you how to get a development env running

Clone the repository onto your machine

```
git clone https://Mtwark@bitbucket.org/Mtwark/superscribe.git
```

## Using Superscribe

Run the application in Android Studio or on your local device

Create an account

Add new subscriptions using the floating action button in the bottom right portion of the main screen

A breakdown of subscription costs will display on the main dashboard

## Future Versions
- Future versions will include the ability to automatically determine subscription status and cost
- Future versions will include the ability to view current benefits for each subscription, as well as alternative subscription tiers
- Future versions will include the ability to upgrade or downgrade subscription plans from within the app
- Future versions will include the ability to cancel subscriptions from within the app

## Built With

* [MPAndroidChart](https://github.com/PhilJay/MPAndroidChart/) - Used to build and animate charts

## Authors

* **Matthew Wark** - *Initial work* - [Mtwark](https://bitbucket.com/Mtwark)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


