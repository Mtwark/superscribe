package com.example.superscribe;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, AddSubscriptionActivity.class);
                startActivity(intent);
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        navigationView.setNavigationItemSelectedListener(this);

        drawPieChart();
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_settings) {
            Intent intent = new Intent(DashboardActivity.this, SettingsActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {
            Toast.makeText(this, "You have successfully logged out", Toast.LENGTH_SHORT).show();
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
            startActivity(intent);
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // Builds the subscription piechart and calls the method to draw the value in the center of the chart
    public void drawPieChart() {

        PieChart pieChart = findViewById(R.id.piechart);

        // Piechart setup
        pieChart.setUsePercentValues(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5,10,5,5);
        pieChart.setDragDecelerationFrictionCoef(.95f);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);
        pieChart.setTransparentCircleRadius(91f);

        ArrayList<PieEntry> yvalues = new ArrayList<>();

        // ADDING TEST DATA
        float[] values = {12, 39, 10, 80, 8, 10};
        float total = 0;
        for (int i = 0; i < values.length; i++) {
            total += values[i];
        }

        yvalues.add(new PieEntry(values[0], "Netflix"));
        yvalues.add(new PieEntry(values[1], "24 Hr Fitness"));
        yvalues.add(new PieEntry(values[2], "Hulu"));
        yvalues.add(new PieEntry(values[3], "Internet"));
        yvalues.add(new PieEntry(values[5], "Spotify"));

        // Chart animations
        pieChart.animateY(2000, Easing.EaseInOutCubic);
        startCountAnimation((int)total);

        // Setup piechart labels and spacing between slices
        PieDataSet dataset = new PieDataSet(yvalues, "");
        dataset.setSliceSpace(3f);
        dataset.setSelectionShift(10f);


        // Setup lines to draw labels outside of the chart
        //dataset.setValueLinePart1OffsetPercentage(80f);
        //dataset.setValueLinePart1Length(0.4f);
        //dataset.setValueLinePart2Length(0.1f);
        //dataset.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        // Set chart data
        PieData data = new PieData(dataset);
        data.setValueTextSize(10f);
        pieChart.setData(data);

        // Set chart colors
        dataset.setColors(ColorTemplate.VORDIPLOM_COLORS);
        data.setValueTextColor(Color.BLACK);
        pieChart.setEntryLabelColor(Color.BLACK);
        pieChart.setDrawEntryLabels(false);

    }

    // Animation for the total cost of all subscriptions
    private void startCountAnimation(int total) {
        final TextView totalTextView = findViewById(R.id.totalTextView);
        ValueAnimator animator = ValueAnimator.ofInt(0, total);
        animator.setDuration(2100);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(@NonNull ValueAnimator animation) {
                totalTextView.setText("$" + animation.getAnimatedValue().toString());
            }
        });
        animator.start();
    }
}
