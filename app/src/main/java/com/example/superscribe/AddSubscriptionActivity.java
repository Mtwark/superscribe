package com.example.superscribe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddSubscriptionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_subscription);
        Button submitButton = findViewById(R.id.submitSubButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText subName = findViewById(R.id.subNameEditText);
                EditText subCost = findViewById(R.id.subCostEditText);
                String name = subName.getText().toString();
                float cost = Float.parseFloat(subCost.getText().toString());
                finish();
            }
        });
    }
}
